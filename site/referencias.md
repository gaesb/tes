# Referencias

- H. Laczkó, B. Jánossy and T. Zsedrovits, "Towards 3D Cave Mapping with UAVs," 2021 17th International Workshop on Cellular Nanoscale Networks and their Applications (CNNA), Catania, Italy, 2021, pp. 1-4, doi: 10.1109/CNNA49188.2021.9610810

- Sistema híbrido de topografía espeleológica. Su aplicación en la nueva topografía de la cueva de la Pileta (Benaoján, Málaga)
  https://www.researchgate.net/publication/323604609_Sistema_hibrido_de_topografia_espeleologica_Su_aplicacion_en_la_nueva_topografia_de_la_cueva_de_la_Pileta_Benaojan_Malaga

- Yavor SHOPOV, Ognian OGNIANOV & Krisia PETKOVA. Remote Location of unknown caves by Thermal Cameras.
  - https://www.youtube.com/watch?v=niusPUXjUrY
  - https://www.youtube.com/watch?v=GGYNIqrRhP0
