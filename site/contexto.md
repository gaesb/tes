# Contexto

Debido a la dificultad de movilidad y la duración de las baterias, la potencia de los dispositivos disponibles en punta,
en el vivac o entrada, y en gabinete son diferentes.
Aunque hoy en día los dispositivos móviles (smartphones) tienen potencia suficiente para realizar todas las tareas
(incluyendo la visulización en 3D o GIS), hay algunas que no es necesario realizar en punta y pueden hacerse mejor
en un punto de descanso o al volver a gabinete.

## En punta

Cuando se inició la digitalización de la topología espeleológica se utilizaban PDAs que tenían sistemas operativos como
Windows Mobile, Palm OS, Symbian, etc.
Android no existía aún, o los dispositivos tenían un consumo notablemente mayor.

En aquel contexto, algunas de las aplicaciones más utilizadas eran PocketTopo y {doc}`sw/auriga`.
En la página de Beat Heeb (desarrollador original de DistoX), se indica:

> PocketTopo is a .Net Compact Framework application running on most Windows Mobile.
> The application also runs in a Mobile Device Simulator or directly on a Windows PC with the .Net Framework installed.
> PocketTopo does not run on Palm devices. Palm owners may use the Auriga application instead.

{doc}`sw/auriga` es probablemente la referencia.
Como se indica en la web, está desarrollada para Palm OS, pero hoy en día se puede emular en Android y Windows Mobile.
Soporta transferencia bidireccional de datos con {doc}`sw/visualtopo`, entre otras aplicaciones.
Desconozco si soporta la conexión con DistoX, BRIC o SAP; o si por el contrario está pensada para la introducción
manual de los datos.

Entre las aplicaciones nativas para Android encontramos {doc}`sw/topodroid` y {doc}`sw/sexytopo`.
Ambas soportan DistoX (multiples modelos), BRIC y SAP; de forma que las medidas pueden transferirse al momento (por
Bluetooth).
Ambas son *open source* (GPL) y las fuentes están en GitHub.

TopoDroid es la aplicación que usamos en el GAES.
No tenemos mayores referencias sobre SexyTopo.
No obstante, puesto que soporta los mismos dispositivos de medida, sería interesante probarla.

### PDA, smatphone o tablet

Actualmente en el GAES se toman las medidas de la poligonal, radiales y auxiliares utilizando el {doc}`hw/distox` y un
smartphone con {doc}`sw/topodroid`, y se realiza el boceto del contorno en papel.
Después, al salir o en el vivac, se limpia/corrige el proyecto en TopoDroid de acuerdo con las anotaciones en papel y se
exporta a {doc}`sw/visualtopo` y {doc}`sw/therion`.
Aunque TopoDroid permite dibujar, no es una característica que usemos por ahora.

Por lo tanto, al no estar utilizando la potencia de cálculo ni la resolución de pantalla del smartphone, lo ideal sería
utilizar una PDA con pantalla monocromo, como las que existían antes de que surgieran las Blackberry y los Smartphone.
Las pantallas a color y con altísima resolución son lo que más consume en los dispositivos actuales, y los procesadores
de los smartphones actuales están sobredimensionados para las funcionalidades necesarias en la topografía espeleológica.
Sin embargo, actualmete Android domina el mercado y resulta muy difícil encontrar dispositivos tipo PDA, smartphone o
tablet que no estén basados en el mismo.
Además, las aplicaciones más activamente desarrolladas (aunque no las más longevas), como son TopoDroid o SexyTopo,
están escritas en Java y pensadas para su uso en Android.

A mayores, el peso del sistema operativo en sí no es ligero precisamente, por lo que a pesar de los pocos requerimientos
de las aplicaciones para topografía, los dispositivos de gama baja es probable que den problemas por falta de RAM.
Asimismo, los dispositivos de gama media y alta permiten configurar el espacio de colores de la pantalla para que se use
escala de grises, pero desconozco si es posible también en los de gama baja.
En este contexto, parece necesario asumir que el smartphone deberá ir acompañado de un *powerbank* para evitar que una
jornada de exploración se vea interrumpida por falta de batería.
Hemos realizado sesiones de 8-10h consumiendo solo el 35-40% de la batería, pero es de esperar una degradación y que
hagamos mayor uso conforme aprendamos características avanzadas.

Si quisiéramos probar a bocetar en digital directamente, el tamaño ideal de pantalla parece ser entre 8 y 10 pulgadas,
ya que un tamaño menor hace demasiado lento el dibujado y un tamaño mayor es complicado de transportar por gateras y
laminadores.
Desconozco cómo funcionan los boligrafos digitales, si funcionan con cualquier pantalla táctil (cualquier smartphone o
tablet) o si el dispositivo tiene que tener alguna pantalla en particular para que la resolución sea aceptable.

En tamaños a partir de 8 pulgadas es posible encontrar dispositivos con Windows 10 u 11.
Sin embargo, el consumo de Windows y de los procesadores Intel es notablemnte mayor que el de Android y ARM, por lo que
no resultan recomendables.
Además, las aplicaciones más desarrolladas en la actualidad están pensadas para Android.

## In-situ

En el vivac o en la entrada, aunque el suministro eléctrico puede ser limitado también, es de esperar que haya más
espacio y tiempo para poder trabajar en los datos con mayor comodidad.
En este contexto, resulta interesante disponer de una tablet o portatil de 11-14 pulgadas, donde poder visualizar mejor
la cueva en su conjunto.

### Android

Las caracteristicas de visualización de TopoDroid pueden ser adecuadas y suficientes.

{doc}`sw/leaflet` permite visualizar capas SIG personalizadas en navegadores web offline.

{doc}`sw/qfield` permite cargar capas complejas exportadas de {doc}`sw/qgis`.

### Windows

{doc}`sw/visualtopo` es la herramienta usada actualmente en el GAES.
Sin embargo, es posible que conforme aprendamos sobre TopoDroid y otras herramientas, el uso de VisualTopo se reduzca.

Si es necesario solapar los datos de la cueva con otras capas SIG, puede ser interesante disponer de {doc}`sw/qgis`.

## En gabinete

Las mismas aplicaciones que se usan in-situ pueden usarse en gabinete: TopoDroid, Visual Topo, Leaflet, QGIS...

Adicionalmente, se utiliza AutoCAD para redibujar en formato vectorial los croquis dibujados a mano sobre la poligonal
con contornos extraída de VisualTopo.

Se pueden realizar renderizados/vídeos 3D con {doc}`sw/blender`, pero no tengo claro qué herramientas
se utilizan para obtener el contorno de las galerías y salas en 3D con mayor realismo que el resultado de VisualTopo.
