Topografía Espeleológica Digital
################################

Como en otros muchos nichos de difícil rendimiento económico, la digitalización de la espeleología es tardía y de
producción interna.
Aunque existen propuestas electrónicas desde los 90, hace poco más de una década que existen soluciones adecuadas para
tomar medidas topográficas en punta utilizando sólo dispositivos digitales, y todavía gran parte de la comunidad sigue
usando soluciones analógicas (papel y lapiz).

En el artículo `Topografía Subterránea Digital 3.0 (PDF) <TSD30.pdf>`_ y en este sitio web se recoge información sobre
dispositivos hardware y aplicaciones software para la consecución de flujos de trabajo íntegramente o parcialmente
digitales y preferiblemente automatizados.


.. toctree::
  contexto


.. toctree::
  :caption: Flujos

  flows/1.0
  flows/2.0
  flows/3.0
  flows/4.0


.. toctree::
  :caption: Hardware

  hw/distox
  hw/bric
  hw/sap
  hw/caveatron
  hw/uev


.. toctree::
  :caption: Software (Campo)

  sw/topodroid
  sw/auriga
  sw/sexytopo
  sw/toporobot
  sw/qave
  sw/qfield


.. toctree::
  :caption: Software (Gabinete)

  sw/visualtopo
  sw/therion
  sw/survex
  sw/csurvey
  sw/cavewhere
  sw/compass
  sw/tunnel
  sw/ghtopo
  sw/walls
  sw/qgis
  sw/leaflet
  sw/caveview
  sw/blender


.. toctree::
  :caption: Apéndice

  referencias
