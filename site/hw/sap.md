# Shetland Attack Pony (SAP)

Aunque no hay muchas referencias al respecto en la red, el SAP ([shetlandattackpony.co.uk](https://www.shetlandattackpony.co.uk/))
parece ser un dispositivo de bajo coste y aparentemente completamente *open source* de un desarrollador británico,
Phil Underwood.

Las especificaciones son similares a las del DistoX.
La pantalla es más pequeña, lo cual es favorable de cara al uso de la batería pero puede dificultar la lectura.

Está basado en un [XIAO BLE Sense](https://wiki.seeedstudio.com/XIAO_BLE/), que es una pequeña placa con un nRF52840 e
incluye acelerómetro y giróscopo, de forma que lo único a añadir son el magnetómetro, el sensor de distancia y el display.
Es llamativo que el firmware del SAP6 está escrito en Python (MicroPython/ircuitPython), mientras que el del SAP5 estaba
escrito en C.

Hay varios repositorios en GitHub.
El del SAP5 parece estar bastante completo, pero estaba basado en un PIC.
El del STIC (que parece ser el SAP6) no parece estar del todo completo; por ejemplo, hay un proyecto de KICAD pero está
sin rutar.
No obstante, se ve por la actividad en el repositorio que está trabajando en ello.

## Referencias

- [shetlandattackpony.co.uk](https://www.shetlandattackpony.co.uk/)
- [gh:furbrain](https://github.com/furbrain)
  - [gh:furbrain/STIC](https://github.com/furbrain/STIC)
  - [gh:furbrain/SAP5](https://github.com/furbrain/SAP5)
  - [gh:furbrain/argyllotter](https://github.com/furbrain/argyllotter)
- [Calibrating the Shetland Attack Pony](https://www.youtube.com/watch?v=q_M8_QxNhjs)
- [yt:w7LjV2FyLJI](https://www.youtube.com/watch?v=w7LjV2FyLJI)
- [thing:5783188](https://www.thingiverse.com/thing:5783188)
