# Caveatron

- {web}`caveatron.com`
  - The Caveatron: An Integrated Cave Survey and LIDAR Scanning Instrument, Joe Mitchell and Steve Gutting, Proceedings of the 17th International Congress of Speleology
  - {gh}`Caveatron/Caveatron`
