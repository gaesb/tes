# DistoX | DistoX2 | DistoXBLE

```{important}
Ver [gitlab.com/euskalespeleo/uev-x](https://gitlab.com/euskalespeleo/uev-x).
```

## Contexto

El dispositivo de toma de medidas usado internacionalmente (al menos en Europa) parece ser DistoX.

- De acuerdo con [derekbristol.com/disto](https://www.derekbristol.com/disto), el DistoX original lo desarrolló Beat
  Heeb en 2008.
  Se trataba de un PCB para reemplazar la placa principal de un medidor de distancia láser comercial, concretamente el
  Leica Disto A3, y añadir un magnetómetro y un acelerómetro que permitieran medir los ángulos (acimut e inclinación) al
  mismo tiempo que se mide la distancia, además de incluir conexión Bluetooth.
- En 2013, el mismo desarrollador creó el DistoX2, pensado para ser montado en el Leica Disto e7400x|x310.
  El firmware siguió actualizándose hasta la versión 2.5 ya que desde 2013 hasta 2016 hubo algunos cambios en los
  componentes tanto del Leica Disto e7400x|x310 como del DistoX2.
- En 2019 Heeb comunicó que no seguiría fabricando la placa, ya que algunos componentes estaban obsoletos y no disponibles
  ([ukcaving.com/board/index.php?threads/distox2-no-longer-available.25870](https://ukcaving.com/board/index.php?threads/distox2-no-longer-available.25870/)).
- En 2020, Oliver Landolt realizó una variación del DistoX2 que se denominó versión 1.2, para cambiar los componentes
  obsoletos.

En [paperless.bheeb.ch](https://paperless.bheeb.ch/) (aparentemente la página de Beat Heeb), más concretamente en [paperless.bheeb.ch/download.html#Hardware](https://paperless.bheeb.ch/download.html#Hardware),
se pueden descargar las fuentes de fabricación del DistoX (DistoX1), DistoX2 v1.1 y DistoX2 v1.2.
Nótese que DistoX2 v1.1 está fechado el 2019/05/28, a pesar de que la última versión del firmware es del 2016/08/30.
Por otro lado, el firmware incluido en el zip de la v1.2 es la v2.6.1.
Es posible que Heeb adaptara el firmware para que funcionara con las placas de Oliver.
En esa misma página se indica que:

> *Oliver will produce and distribute a limited number of upgrade kits.*
> *To limit his workload, cavers and caving clubs are encouraged to make their own boards.*

Los manuales de usuario, montaje y calibración del DistoX2 están en [paperless.bheeb.ch/download.html#DistoX2](https://paperless.bheeb.ch/download.html#DistoX2).

Tras el número limitado de kits que hizo Oliver, la comunidad quedó sin continuidad hasta que en 2023 Siwei Tian diseñó
una nueva versión denominada DistoXBLE o DistoX2 v1.3.
En una nota de Oliver publicada en [souterweb.free.fr/boitaoutils/topographie/pages/disto.htm](http://souterweb.free.fr/boitaoutils/topographie/pages/disto.htm),
se explica que la versión v1.3 está basada en BLE en vez de Bluetooth Classic utilizado en las versiones anteriores,
debido a la creciente dificultad para encontrar módulos BT compatibles con el modo Classic.
En la misma nota se indica que Siwei se tomó el tiempo para rediseñar en profundidad el PCB y el firmware.
Sin embargo, a diferencia de las versiones anteriores, parece ser que las fuentes de fabricación de la versión v1.3 y el
firmware no están disponibles a la comunidad.
Se puede contactar con Siwei por correo electrónico para comprar kits, pero no fabricarlos localmente;
como explícitamente propuso Oliver cuando hizo la v1.2.

En Abril de 2024 los precios de Siwei son:

- Distox2 completo montado y calibrado: 470€
- Calibrator: 30€
- Placa: 190€
- Bateria: 40€
- Leica DistoX310: 180€

En la nota de Oliver se indica que Siwei colaboró con Marco Corvi (desarrollador de TopoDroid) y en [ukcaving.com/board/index.php?threads/distoxble-users.30870](https://ukcaving.com/board/index.php?threads/distoxble-users.30870/)
se indica que colaboró con Rich Smith (desarrollador de SexyTopo) para que el protocolo BLE funcione en las aplicaciones
para Android.

## Firmware y copyright

El medidor Leica tiene dos PCBs: uno para el sensor y uno como controlador principal, al que está conectado el LCD.
El DistoX (en cualquiera de sus versiones) reemplaza la placa principal.
Por lo tanto, además de interactuar con los sensores, el DistoX controla el LCD y los botones.

No es trivial desarrollar el firmware para gestionar el LCD y el sensor de distancia, además de los sensores añadidos y
el Bluetooth.
El hecho de que el firmware parece estar disponible sólo en formato compilado invita a pensar que se realizó a partir
del código fuente original del Leica.
Podría haberse realizado mediante ingeniería inversa, suponiendo que se realizara en un país donde sea legal; pero parece
improbable porque sería un trabajo muy tedioso.
Más aún, puesto que las fuentes del DistoX y DistoX2 están disponibles y se invitaba a otros a fabricarlo, resulta
llamativo que las fuentes del firmware no se pusieran a disposición de la misma forma.

Aunque no he encontrado referencias explícitas, se me ha hecho saber que Heeb trabajaba en Leica cuando desarrolló el
primer DistoX; y que fue despedido ya que a Leica no le gustó la idea.
El siguiente aviso en la página de Heeb parece reforzar lo anterior:

> This work is not related to Leica Geosystems AG.
> I do not provide complete devices. You have to buy a Disto and modify it yourself.
> Opening and modifying a Leica Disto will void the manufacturer’s warranty and is done at your own risk!
> Don’t bother Leica with questions about the DistoX!

Desde un punto de vista legal, no se puede impedir la fabricación y distribución de los PCBs DistoX, y muy probablemente
sea imposible demostrar que el firmware compilado se corresponde con las fuentes de Leica; pero Heeb podría tener
problemas legales si se distribuyera el código fuente y proviniera de Leica.

Asumiendo que el firmware de las versiones v1.0, v1.1 y v1.2 la realizó Heeb sin que el código saliera de su equipo,
queda la duda sobre cómo desarrolló Siwei el de la versión v1.3.
Es llamativo que la página web de Heeb no incluya ninguna referencia a dicha versión.

Desde el punto de vista de la fiabilidad, sería conveniente que el código de Siwei estuviera basado en el de Heeb.
Sin embargo, sin ninguna información sobre la lista de componentes o el esquemático de la v1.3 es prácticamente
imposible saber.
Por experiencias similares en otras áreas, es probable que sea un código escrito desde cero, con un gran esfuerzo
técnico pero sin el debido tiempo de depuración y pruebas; y por tanto propenso a fallos ([ukcaving.com/board/index.php?threads/distoxble-users.30870](https://ukcaving.com/board/index.php?threads/distoxble-users.30870/)).
Muchos posibles problemas podrían solucionarse si el código estuviera disponible, pero el hecho de que sólo se pueda
contactar por correo y parezca no haber ninguna página web o repositorio, invita a pensar que no es la intención de Siwei.

## Mantenimiento y obsolescencia

Además de los manuales disponibles en la página de Heeb, hay enlaces a varios vídeos en [paperless.bheeb.ch/links.html](https://paperless.bheeb.ch/links.html)
y otros más actuales como [yt: How to modify Leica Disto x310 upgrade with Disto x2 and Disto xble](https://www.youtube.com/watch?v=6OvoH-lpF4c) donde se meciona también el DistoXBLE.
Hay varios documentos del montaje de la versión DistoXBLE:

- [archive.org/details/disto-xble-assembly](https://archive.org/details/disto-xble-assembly/DistoXBLE-assembly/mode/2up)
- [catastogrotte.net/liguria/en/bibliography/view/2196/?nv_year=2023&page=1](https://www.catastogrotte.net/liguria/en/bibliography/view/2196/?nv_year=2023&page=1)

Si algún mal funcionamiento se debiera a algún condensador en mal estado, sería sencillo reemplazarlo en los
modelos v1.0, v1.1 y v1.2.
Sin embargo, en lo que respecta a problemas de programación o funcionamiento errático del microcontrolador, resulta
muy difícil de afrontar sin acceso al código fuente.

Por otro lado, como se indica en [souterweb.free.fr/boitaoutils/topographie/pages/disto.htm](http://souterweb.free.fr/boitaoutils/topographie/pages/disto.htm) y se puede comprobar en [disto.es/X310/disto_x310.php](https://www.disto.es/X310/disto_x310.php)
el Leica Disto X310 está descatalogado.
Siendo el único modelo para el que están pensados los DistoX2 y DistoXBLE, es de esperar que el precio de las pocas
unidades que existan se vaya incrementando hasta un coste no razonable.

En Abril de 2024, Siwei parece tener stock de Disto x310, a 180€.
Si algún grupo pudiera obtenerlo por otra vía, hay información suficiente para comprar el kit de Siwei y realizar la
modificación/instalación.

No obstante, resulta relativamente urgente para la comunidad encontrar una alternativa con un coste razonable (menos de
500€ el kit de medidor más placa de expansión).
Ver [gitlab.com/euskalespeleo/uev-x](https://gitlab.com/euskalespeleo/uev-x).

## Calibrator de Ortzi

Puesto que el Leica x310 no está específicamente pensado para su uso en cavidades, puede resultar complicado el
posicionamiento en puntos/nodos situados en formas lisas o cóncavas.
Con intención de mejorar la precisión de las topografías en cavidades, además de facilitar y mejorar las calibraciones,
en 2015 Israel Robles del G. E. Takomano diseñó una carcasa con un *pincho* de 5 cm en la base.
Tanto para el DistoX2 como para el DistoX.

```{note}
En la carcasa puede leerse en relieve *CALIBRATOR* y *ORTZI*.
No confundir con la *estrella* de calibración de Mykyta (ver más abajo) u otros dispositivos similares pensados sólo
para la calibración.
```

Para su diseño, tuvo en cuenta una característica del propio DistoX: la posibilidad de tomar las medidas desde la
ubicación del sensor (el frente del dispositivo) o desde el trípode (situado a cierta distancia de la base del dispositivo).
A partir de la versión 2.4 del firmware, se puede configurar la distancia de la base al trípode;
en el caso de la carcasa, a la punta del *pincho*.

Nótese que la posición del *pincho* está muy bien ajustada con respecto a la posición del láser en el interior del
DistoX, ya que de lo contrario se introduciría error en las medidas.
Isra comenta que es una de las cosas que más le costó ajustar del diseño.

Lo fabrica en impresora 3D y está disponible por 30€, incluyendo instrucciones, recambio para el *pincho* y recambio de
la pieza que mantiene el DistoX en posición dentro del Calibrator.

```{important}
Isra comentó que tiene intención de subir las fuentes a Thingiverse.
Cuando estén, añadir el enlace aquí.
Añadir también alguna foto de la carcasa con el pincho (primera figura del artículo TSD 3.0).
Preguntar a Isra si puede facilitar también las fuentes de las hojas de instrucciones/manual que incluye con la carcasa.
```

## Carcasa estanca

Para el transporte del DistoX en cavidades inundadas, pueden usarse cajas estancas de tamaño suficiente, como la
PELICAN 1030.

También hay algún diseño más específico, como por ejemplo [facebook.com/DistoX2Case](https://www.facebook.com/DistoX2Case).

```{note}
Isra mencióno que también diseñó carcasas estancas de transporte y protección para DistoX2 y DistoX.
Pero no he visto fotografías y tampoco le he preguntado más específicamente, aún.
```

## Calibrator de Mykyta y otros

Puesto que la calibración del DistoX requiere la toma de múltiples medidas con el dispositivo en varias posiciones,
existen diseños para facilitar la tarea y reducir el error humano:

- {bb}`ngry/distox2_cube`
  - {thing}`3776523`

- {thing}`4878750`

## Referencias

- [paperless.bheeb.ch/download.html#DistoX2](https://paperless.bheeb.ch/download.html#DistoX2)

- [mondaycavers.com/disto.html](https://mondaycavers.com/disto.html)
  - [mondaycavers.com/disto-using.html](https://mondaycavers.com/disto-using.html)
  - [mondaycavers.com/disto-calibration.html](https://mondaycavers.com/disto-calibration.html)
  - [mondaycavers.com/disto-survey.html](https://mondaycavers.com/disto-survey.html)
  - [mondaycavers.com/disto-topodroid.html](https://mondaycavers.com/disto-topodroid.html)

- [gzluna.com/cave/distocalibnew.html](http://gzluna.com/cave/distocalibnew.html)

- [The DistoX2: A methodological solution to archaeological mapping in poorly accessible environments](https://www.sciencedirect.com/science/article/pii/S2352409X2030479X)

- Paperless Caving - An Electronic Cave Surveying System La topo sans papier - un système électronique de topographie, Beat HEEB, 4th European Speleological Congress
- An All-In-One Electronic Cave Surveying Device, Beat HEEB, Switzerland, July 2009 [CREG 72]
- DistoX Calibration Manual 11/27/2008
- A General Calibration Algorithm for 3-Axis Compass/Clinometer Devices, Beat HEEB, Switzerland, Sept 2009 [CREGJ 73, March 2010]
- The Next Generation of the DistoX Cave Surveying Instrument, Beat HEEB, Switzerland, October 2014 [CREG 88]
- DistoX calibration tools and the need for calibration checking, May 2021, Acta Carsologica / Karsoslovni Zbornik 50(1),  DOI:10.3986/ac.v50i1.9653
- https://arxiv.org/pdf/2102.09891.pdf DISTOX CALIBRATION TOOLS AND THE NEED FOR CALIBRATION CHECKING
- DistoX2-VoR.pdf  The DistoX2: A methodological solution to archaeological mapping in poorly accessible environments
- https://www.scribd.com/document/32521363/Disto-X
