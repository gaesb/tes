# UEV

Aunque el coste de fabricación de prototipos utilizando componentes comunes no es desorbitado, tampoco es calderilla.
Se necesitan 300-500€ para el desarrollo de cada concepto (asumiendo que las horas de ingeniería son voluntarias).

Al GAES le queda grande la prueba, desarrollo y/o adopción de nuevas herramientas para la toma de medidas.
Dadas las características actuales del grupo y la previsión de actividad durante los próximos meses, nuestros esfuerzos
están mejor enfocados en la modernización de nuestro flujo de trabajo en lo que respecta al software y la gestión de
información.

Sin embargo, la UEV, que aúna un colectivo mayor y perteneciente a muchos grupos/clubs, puede tener entidad suficiente
para invertir en la búsqueda de un reemplazo para el DistoX y/o para el desarrollo/adopción de otras herramientas de
relativamente bajo coste para mejorar la calidad de los modelos topográficos.

Ver {gl}`euskalespeleo/uev-x` y {gl}`euskalespeleo/uev-r`.
