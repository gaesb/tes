# BRIC

Kris Fausnight lleva desde 2010 produciendo versiones del BRIC, que actualmente está en el modelo 5.

Las especificaciones son similares o ligeramente mejores que las del DistoXBLE, y dispone de una pantalla mejor
aprovechada.
Además, los acelerómetros y magnetómetros son redundantes (están duplicados) para detectar derivas y errores.
Tiene calibración integrada, sin necesidad de un dispositivo externo.

Sin embargo los botones y la carcasa no parecen tan robustos/duraderos ya que la carcasa está impresa en 3D (en nylon,
con tapa de policarbonato).
Los modelos anteriores reutilizaban cajas estancas, por lo que los botones estaban en el interior.

Parece un producto cerrado, por lo que no hay disponibles fuentes de fabricación ni manuales.

El precio ($969) es el doble que el Leica y el DistoXBLE.
Es un producto muy interesante, pero el coste es excesivo.

- [bricsurvey.com](https://www.bricsurvey.com/)
- [bricsurvey.com/about](https://www.bricsurvey.com/about)
