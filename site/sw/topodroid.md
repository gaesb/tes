# TopoDroid

## Instalación

Estaba disponible en GooglePlay hasta la versión 5, pero desde la versión 6 hay que descargar el `.apk` de la
página del autor en altervista.
Esto es debido a las limitaciones de Google para que las aplicaciones accedan a ciertos recursos del teléfono.
Para ofrecer ciertas características de acceso y guardado de datos, las nuevas versiones no cumplen todas las limitaciones
y por eso no están en GooglePlay.

A partir de la versión 6, la aplicación se llama TopoDroid-X.
Se pueden tener instaladas ambas, la versión 5 a través de GooglePlay y la versión 6 por medio del `.apk`.

```{important}
A partir de Android 11, algunos dispositivos pueden requerir la habilitación de permisos adicionales.
```

### Portátiles o equipos de gabinete

TopoDroid resulta conveniente en punta, porque podemos usarlo en dispositivos Android como smartphones o tablets.
Sin embargo, en gabinete resulta incómodo tener que estar con un ordenador y un smartphone/tablet al mismo tiempo.

Mediante un emulador Android, podemos instalar y utilizar TopoDroid en portátiles u ordenadores, de forma que podamos
editar el contenido con teclado y ratón, además de tenerlo abierto junto con otras aplicaciones en los mismos monitores.

En Windows, [bluestacks.com/es/android-11.html](https://www.bluestacks.com/es/android-11.html), aunque enfocado en
habilitar el uso de juegos para móvil, es fácil de instalar y TopoDroid funciona.

```{note}
Los grupos que utilicen un solo flujo de trabajo basado en VisualTopo o en Therion pueden no utilizar TopoDroid fuera
de la cavidad, sino exportar el contenido a uno de esos formatos solo.
Sin embargo, los grupos que están transitando de un flujo de trabajo a otro pueden preferir editar el contenido en
TopoDroid para después exportar lo mismo a ambas aplicaciones.
```

## Configuración

TopoDroid muestra/oculta muchas opciones en función del *nivel* (*Activity level*) indicado en la configuración.
Deberemos indicar *Expert*, o *Tester* si queremos usar las opciones de integración de varias sesiones en un proyecto.

Además, en la sección *SURVEY DATA* es recomendable ajustar algunos valores por defecto:

- Qué patrón utilizamos para tomar las medidas (*Stations policy*).
- El nombre del grupo (*Team*).
- Poder editar los nodos/estaciones desde el listado de medidas (*Editable stations*).

En las sesiones, es recomendable seleccionar todos los elementos en *Display mode* (*Shot IDs*, *Splay shots*, *Blank shots* y *Repeated leg shots*).

## Cave3D y gestión de proyectos

Como se indica en [sites.google.com/site/speleoapps](https://sites.google.com/site/speleoapps/), las aplicaciones Cave3D
y TdManager que anteriormente se desarrollaban por separado se han incluido en TopoDroid a partir de la versión 5.
Es muy interesante que se estén integrando las características y que soporte no sólo la adquisición sino también la
visualización.

```{important}
Para utilizar el gestor de proyectos es necesario indicar el nivel de actividad *Tester*, no es suficiente con *Expert*.
Ver [groups.google.com/g/topodroid/c/Z4SjqHLeclk](https://groups.google.com/g/topodroid/c/Z4SjqHLeclk).

Nótese que en [groups.google.com/g/topodroid/c/IVSj2J55ZQU](https://groups.google.com/g/topodroid/c/IVSj2J55ZQU) se
puntualiza que TopoDroid no gestiona la variación de la [declinación](https://es.wikipedia.org/wiki/Declinaci%C3%B3n_magn%C3%A9tica).
```

El gestor de proyectos permite combinar varias sesiones para visualizarlas en conjunto.
Se puede especificar un color diferente para cada sesión y añadir *equates* para indicar la equivalencia de
nodos/estaciones entre sesiones.

No es casual el uso del término *equate* ya que TopoDroid internamente parece utilizar el formato de Therion.
En `Documents/TDX/TopoDroid/thconfig/*.tdconfig` se encuentran los ficheros de configuración de los proyectos.
Los proyectos pueden exportarse a Therion (que sólo copia el fichero `.tdconfig` renombrado a `.th.thconfig`) o a Survex.
No parece posible importar proyectos.

## Exportación

TopoDroid permite exportar las sesiones usando más de una docena de formatos, en función del software objetivo.
Los más interesantes para los flujos de trabajo descritos aquí son:

```{note}
Todos los siguientes son aptos para software de control de versiones.
Los `.tro`, `.trox` y `.th` son texto plano, y los dos ficheros contenidos en los `.zip` también (por lo que pueden
extraerse y añadir los ficheros en vez de el `.zip`).
```

### ZIP

El formato propio de TopoDroid.
Contiene un fichero `manifest` (con información sobre la versión de TopoDroid, la fecha, el nombre, y número de
estaciones/nodos) y un `survey.sql` (las medidas en formato SQL).

El `.zip` puede transferirse por Bluetooth, USB o e-mail a otros dispositivos e importarse para, por ejemplo, editar el
contenido en dispositivos con monitores/pantallas mayores que las del dispositivo usado en punta.

### TRO(X)

Formato de VisualTopo.
El formato `.tro` es un fichero de texto tipo CSV, utilizado tradicionalmente por VisualTopo.
El formato `.trox` es un XML, utilizado por las versiones más recientes de VisualTopo.

Opcionalmente, pueden incluirse los *Splays* e indicarse *LRUD at FROM station*.

### TH

Formato de Therion.
Opcionalmente, pueden incluirse comandos de configuración, *LRUD* e indicarse *Maps before data*.

## Dudas

- ¿Qué significa que las tomas estén en rojo?
- ¿Qué significa que las tomas estén en morado?

## Referencias

- [gh:marcocorvi/topodroid](https://github.com/marcocorvi/topodroid)
- [sites.google.com/site/speleoapps/home/topodroid](https://sites.google.com/site/speleoapps/home/topodroid)
  - [/topodroid-v6](https://sites.google.com/site/speleoapps/home/topodroid/topodroid-v6)
    - [marcocorvi.altervista.org/caving/speleoapps/speleoapks/TopoDroidApks.html](http://marcocorvi.altervista.org/caving/speleoapps/speleoapks/TopoDroidApks.html)
  - [/tododroidacks](https://sites.google.com/site/speleoapps/home/topodroid/tododroidacks)
- [play.google.com/store/apps/details?id=com.topodroid.DistoX](https://play.google.com/store/apps/details?id=com.topodroid.DistoX&hl=en)
- [code.google.com/archive/p/topolinux/downloads](https://code.google.com/archive/p/topolinux/downloads)
