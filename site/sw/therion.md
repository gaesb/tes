# Therion

C++, wxWidgets, TCL/Tk, TeX

Disto-X and any of PocketTopo | TopoDroid | SexyTopo | CaveSurvey | Qave | Auriga

- [w:Therion_(software)](https://en.wikipedia.org/wiki/Therion_(software))
- [therion.speleo.sk](https://therion.speleo.sk/)
  - [/download](https://therion.speleo.sk/download.php)
  - [wiki/es:tpe:1](https://therion.speleo.sk/wiki/es:tpe:1)
- [Therion by examples [marcocorvi.altervista.org/caving/tbe]](http://www.marcocorvi.altervista.org/caving/tbe/index.htm)

- [gh:robertxa/pytherion](https://github.com/robertxa/pytherion) (pytro2th)
- [data.oreme.org/karst3d/karst3d_convert](https://data.oreme.org/karst3d/karst3d_convert)
  - [gh:Alexkuark/pytherion](https://github.com/Alexkuark/pytherion)
